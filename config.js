const dotenv = require('dotenv');

dotenv.config();

module.exports = {
  JWT_SECRET: process.env.JWT_SECRET,
  SALT: Number(process.env.SALT),
  PORT: process.env.PORT || 8080,
  DB_URL: process.env.DB_URL,
};
