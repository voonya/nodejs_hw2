const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const {SALT} = require('../config.js');

/**
 *
 */
class UsersController {
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async getUserInfo(req, res) {
    try {
      const {id} = req.user;
      const user = await User.findOne({_id: id},
          {'username': 1, '_id': 1, 'createdDate': 1});
      console.log(user);

      res.status(200).json({
        message: 'Success',
        user: {
          ...user._doc,
        },
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async deleteUser(req, res) {
    try {
      const {id} = req.user;
      await User.deleteOne({_id: id});
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async updateUser(req, res) {
    const {oldPassword, newPassword} = req.body;
    const {id} = req.user;

    if (!oldPassword && !newPassword) {
      return res.status(400).json({
        message: 'Specify oldPassword and newPassword fields!',
      });
    }


    const {password} = await User.findOne({_id: id}, {'_id': 0, 'password': 1});
    console.log(oldPassword, password);
    if (!bcrypt.compareSync(oldPassword, password)) {
      return res.status(400).json({
        message: 'Wrong old password!',
      });
    }

    try {
      const newHashPassword = bcrypt.hashSync(newPassword, SALT);
      const updated = await User.updateOne({_id: id},
          {'password': newHashPassword});

      if (updated.modifiedCount === 0) {
        throw new Error('Didn`t updated user password!');
      }

      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
}

module.exports = new UsersController();
