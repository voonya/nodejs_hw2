const Note = require('../models/Note.js');

/**
 *
 */
class NotesController {
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async getNotes(req, res) {
    console.log('Notes user: ', req.user);
    const {id} = req.user;
    const limit = req.query.limit || 0;
    const offset = req.query.offset || 0;

    try {
      const notes = await Note.find({userId: id},
          {'__v': 0}, {skip: offset, limit});
      res.status(200).json({
        message: 'success',
        offset,
        limit,
        count: notes.length,
        notes,
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async createNote(req, res) {
    const {id} = req.user;
    const {text} = req.body;

    if (!text) {
      return res.status(400).json({
        message: 'Specify text of note!',
      });
    }

    try {
      const newNote = await Note.create({
        userId: id,
        completed: false,
        text,
        createdDate: new Date(),
      });

      console.log(newNote);
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async getNote(req, res) {
    const {id} = req.params;
    const userId = req.user.id;

    try {
      const note = await Note.findOne({_id: id, userId}, {'__v': 0});
      if (!note) {
        return res.status(400).json({
          message: 'This user doesn`t have note with specified id!',
        });
      }
      res.status(200).json({
        message: 'Success',
        note,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async updateTextNote(req, res) {
    const {id} = req.params;
    const userId = req.user.id;
    const {text} = req.body;

    if (!text) {
      return res.status(400).json({
        message: 'Specify text!',
      });
    }

    try {
      const updated = await Note.updateOne({_id: id, userId}, {text});
      if (updated.matchedCount === 0) {
        return res.status(400).json({
          message: 'This user doesn`t have note with specified id!',
        });
      }
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async updateCheckNote(req, res) {
    const {id} = req.params;
    const userId = req.user.id;
    try {
      const updated = await Note.updateOne({_id: id, userId},
          [{'$set': {'completed': {'$not': '$completed'}}}]);
      if (updated.matchedCount === 0) {
        return res.status(400).json({
          message: 'This user doesn`t have note with specified id!',
        });
      }
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
  /**
   *
   * @param {Request} req
   * @param {Response} res
   * @return {Promise<*>}
   */
  async deleteNote(req, res) {
    const {id} = req.params;
    const userId = req.user.id;

    try {
      const deleted = await Note.deleteOne({_id: id, userId});
      if (deleted.deletedCount === 0) {
        return res.status(400).json({
          message: 'This user doesn`t have note with specified id!',
        });
      }
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      res.status(500).json({
        message: 'Server error!',
      });
    }
  }
}

module.exports = new NotesController();
