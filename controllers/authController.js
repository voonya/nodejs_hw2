const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET, SALT} = require('../config.js');


/**
 *
 * @param {string} _id
 * @param {string} username
 * @param {string} createdDate
 * @return {*}
 */
function generateAccessToken({_id, username, createdDate}) {
  return jwt.sign({
    id: _id,
    username,
    createdDate,
  }, JWT_SECRET, {expiresIn: '2h'});
}

// todo: validation username and password middlewares
/**
 *
 */
class AuthController {
  /**
     *
     * @param {Request} req
     * @param {Response} res
     * @return {Promise<*>}
     */
  async register(req, res) {
    const {username, password} = req.body;

    if (!username || !password) {
      return res.status(400).json({
        message: 'Specify username and password!',
      });
    }

    try {
      const hashPassword = bcrypt.hashSync(password, SALT);
      await User.create({
        username,
        password: hashPassword,
        createdDate: new Date(),
      });
      res.status(200).json({
        message: 'Success',
      });
    } catch (err) {
      if (err.code === 11000) {
        return res.status(400).json({
          message: 'User with this username already exist!',
        });
      }
      res.status(500).json({
        message: 'Server error',
      });
      console.log(err);
    }
  }

  /**
     *
     * @param {Request} req
     * @param {Response} res
     * @return {Promise<*>}
     */
  async login(req, res) {
    const {username, password} = req.body;
    const user = await User.findOne({username});
    if (!user) {
      return res.status(400).json({
        message: 'No user with this username!',
      });
    }

    if (bcrypt.compareSync(password, user.password)) {
      const accessToken = generateAccessToken(user);
      return res.status(200).json({
        message: 'Success',
        jwt_token: accessToken,
      });
    }
    res.status(400).json({
      message: 'Invalid password!',
    });
  }
}


module.exports = new AuthController();
