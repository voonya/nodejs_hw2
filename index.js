const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const fs = require('fs');
const globalRouter = require('./routes/globalRouter.js');

const disableCors = require('./middlewares/disableCors.js');

const {DB_URL, PORT} = require('./config');


const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan('common', {
  stream: fs.createWriteStream('./logs.log', {flags: 'a'}),
}));
app.use(morgan('dev'));
app.use('/api', globalRouter);

app.use(disableCors);

const main = async () => {
  try {
    await mongoose.connect(DB_URL)
        .then(
            console.log('Connected to db!'),
        );
    app.listen(PORT, () => {
      console.log('Server started on port: ', PORT);
    });
  } catch (err) {
    console.log(err);
  }
};


main();
