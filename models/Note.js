const {Schema, model} = require('mongoose');

const Note = new Schema({
  userId: {type: Schema.ObjectId, required: true},
  completed: {type: Boolean, required: true, default: false},
  text: {type: String, required: true},
  createdDate: {type: Date, required: true},
});

module.exports = model('Note', Note);
