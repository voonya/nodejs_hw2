const User = require('../models/User.js');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config.js');
/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {function} next
 * @return {Promise<*>}
 */
async function authCheck(req, res, next) {
  const accessToken = req.headers.authorization?.split(' ')[1];
  console.log('AccessToken: ', accessToken);
  try {
    const user = jwt.verify(accessToken, JWT_SECRET);
    req.user = user;
    const userDB = await User.findOne({_id: user.id});
    if (!userDB) {
      return res.status(400).json({
        message: 'No user with this credentials',
      });
    }
  } catch (err) {
    console.log(err);
    if (err.message === 'invalid token' ||
        err.message === 'jwt must be provided' ||
        err.message === 'jwt expired') {
      return res.status(401).json({
        message: err.message,
      });
    }
    return res.status(500).json({
      message: 'Server error!',
    });
  }
  next();
}

module.exports = authCheck;
