/**
 *
 * @param {Request} req
 * @param {Response} res
 * @param {function} next
 */
function disableCors(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept');
  next();
}

module.exports = disableCors;
