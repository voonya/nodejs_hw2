const router = require('express').Router;
const AuthController = require('../controllers/authController.js');

const authRouter = router();

authRouter.post('/register', AuthController.register);
authRouter.post('/login', AuthController.login);

module.exports = authRouter;
