const router = require('express').Router;
const UsersController = require('../controllers/usersController.js');

const usersRouter = router();

usersRouter.get('/me', UsersController.getUserInfo);
usersRouter.delete('/me', UsersController.deleteUser);
usersRouter.patch('/me', UsersController.updateUser);

module.exports = usersRouter;
