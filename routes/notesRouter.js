const router = require('express').Router;
const notesController = require('../controllers/notesController.js');

const notesRouter = router();

notesRouter.get('/', notesController.getNotes);
notesRouter.post('/', notesController.createNote);

notesRouter.get('/:id', notesController.getNote);
notesRouter.put('/:id', notesController.updateTextNote);
notesRouter.patch('/:id', notesController.updateCheckNote);
notesRouter.delete('/:id', notesController.deleteNote);

module.exports = notesRouter;
