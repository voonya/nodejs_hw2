const router = require('express').Router;
const authRouter = require('../routes/authRouter.js');
const notesRouter = require('../routes/notesRouter.js');
const usersRouter = require('../routes/usersRouter.js');
const authCheck = require('../middlewares/authMiddleware.js');

const globalRouter = router();

globalRouter.use('/auth', authRouter);
globalRouter.use('/users', authCheck, usersRouter);
globalRouter.use('/notes', authCheck, notesRouter);

module.exports = globalRouter;
